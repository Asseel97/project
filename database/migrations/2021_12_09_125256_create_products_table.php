<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->mediumText('description');
            $table->mediumText('specification');
            $table->integer('stock');
            $table->foreignId('category_id')->constrained()
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('productstate_id')->constrained()
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('color_id')->constrained()
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('type_id')->constrained()
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('brand_id')->constrained()
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
