<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // cache leegmaken van Permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // permissions pricetype CRUD
        Permission::create(['name'=> 'index pricetype']);
        Permission::create(['name'=> 'show pricetype']);
        Permission::create(['name'=> 'create pricetype']);
        Permission::create(['name'=> 'edit pricetype']);
        Permission::create(['name'=> 'delete pricetype']);
        // permissions addresstype CRUD
        Permission::create(['name'=> 'index addresstype']);
        Permission::create(['name'=> 'show addresstype']);
        Permission::create(['name'=> 'create addresstype']);
        Permission::create(['name'=> 'edit addresstype']);
        Permission::create(['name'=> 'delete addresstype']);
        // permissions brand CRUD
        Permission::create(['name'=> 'index brand']);
        Permission::create(['name'=> 'show brand']);
        Permission::create(['name'=> 'create brand']);
        Permission::create(['name'=> 'edit brand']);
        Permission::create(['name'=> 'delete brand']);
        // permissions color CRUD
        Permission::create(['name'=> 'index color']);
        Permission::create(['name'=> 'show color']);
        Permission::create(['name'=> 'create color']);
        Permission::create(['name'=> 'edit color']);
        Permission::create(['name'=> 'delete color']);

        // customer role
        $customer = Role::create(['name' => 'customer']);

        // sales role
        // pricetype, addresstype, brand, color.
        $sales = Role::create(['name' => 'sales'])
            ->givePermissionTo(['index pricetype', 'show pricetype', 'create pricetype', 'edit pricetype',
                                'index addresstype', 'show addresstype', 'create addresstype', 'edit addresstype',
                                'index brand', 'show brand', 'create brand', 'edit brand',
                                'index color', 'show color', 'create color', 'edit color']);

        // admin role
        $admin = Role::create(['name' => 'admin'])
            ->givePermissionTo(Permission::all());
    }
}
