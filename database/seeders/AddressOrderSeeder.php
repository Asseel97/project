<?php

namespace Database\Seeders;

use App\Models\Addressorder;
use Illuminate\Database\Seeder;

class AddressorderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Addressorder::factory()
        ->times(20)
        ->create();
    }
}
