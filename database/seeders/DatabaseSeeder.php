<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BrandSeeder::class,
            TypeSeeder::class,
            ColorSeeder::class,
            ProductstateSeeder::class,
            PricetypeSeeder::class,
            StateSeeder::class,
            AddresstypeSeeder::class,
            RoleAndPermissionSeeder::class,
            LevorderSeeder::class,
            LevorderstateSeeder::class,
            CategorySeeder::class,
            UserSeeder::class,
            AddressSeeder::class,
            OrderSeeder::class,
            AddressorderSeeder::class,
            ProductSeeder::class,
            ReviewSeeder::class,
            OrderrowSeeder::class,
            PriceSeeder::class,
            PictureSeeder::class,
            LevorderrowSeeder::class

            
        ]);
    }
}
