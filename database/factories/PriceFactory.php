<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Pricetype;
use App\Models\Price;
use Illuminate\Database\Eloquent\Factories\Factory;

class PriceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Price::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'price' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 99999),
            'effdate' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'product_id' => Product::all()->random()->id,
            'pricetype_id' => Pricetype::all()->random()->id,
            'created_at' => $this->faker->dateTimeThisDecade('now', 'Europe/Amsterdam'),
            'updated_at' => $this->faker->dateTimeThisDecade('now', 'Europe/Amsterdam')
        ];
    }
}
