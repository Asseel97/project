<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Address;
use App\Models\Addressorder;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressorderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Addressorder::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'order_id' => Order::all()->random()->id,
            'address_id' => Address::all()->random()->id,
            'created_at' => $this->faker->dateTimeThisDecade('now', 'Europe/Amsterdam'),
            'updated_at' => $this->faker->dateTimeThisDecade('now', 'Europe/Amsterdam')
        ];
    }
}
