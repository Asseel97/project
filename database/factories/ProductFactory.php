<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Productstate;
use App\Models\Color;
use App\Models\Type;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'description' => $this->faker->realText,
            'specification' => $this->faker->userAgent,
            'stock' => $this->faker->randomDigit,
            'category_id' => Category::all()->random()->id,
            'productstate_id' => Productstate::all()->random()->id,
            'color_id' => Color::all()->random()->id,
            'type_id' => Type::all()->random()->id,
            'brand_id' => Brand::all()->random()->id,
            'created_at' => $this->faker->dateTimeThisDecade('now', 'Europe/Amsterdam'),
            'updated_at' => $this->faker->dateTimeThisDecade('now', 'Europe/Amsterdam')
        ];
    }
}
