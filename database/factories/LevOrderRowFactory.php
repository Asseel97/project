<?php

namespace Database\Factories;

use App\Models\Levorder;
use App\Models\Levorderstate;
use App\Models\Product;
use App\Models\Levorderrow;
use Illuminate\Database\Eloquent\Factories\Factory;

class LevorderrowFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Levorderrow::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'amount' => $this->faker->randomDigit,
            'expected' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'levorder_id' => Levorder::all()->random()->id,
            'levorderstate_id' => Levorderstate::all()->random()->id,
            'Product_id' => Product::all()->random()->id,
            'created_at' => $this->faker->dateTimeThisDecade('now', 'Europe/Amsterdam'),
            'updated_at' => $this->faker->dateTimeThisDecade('now', 'Europe/Amsterdam')
        ];
    }
}
