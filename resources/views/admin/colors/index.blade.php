@extends('layouts.index')


@section('content')

    <div class="container mx-1">
        <div class="ml-2 flex flex-col">
            <h2 class="my-4 text-4xl font-semibold text-gray-600 dark:text-gray-400">
                Color
            </h2>
        </div>
    </div>

<tbody class="bg-white divide-y divide-gray-200">
    @foreach($colors as $color)
    <label>
        <td class="px-6 py-4 whitespace-nowrap text-sm font-big text-gray-900">
            {{$color->name }}
        </td>

    </label>
    @endforeach
    </tbody>

