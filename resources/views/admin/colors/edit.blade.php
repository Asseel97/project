<form id="form" class="bg-white"
      action="{{route('colors.update',['color' =>$color->id])}}" method="POST">
    @method('PUT')
    @csrf
    <div class="mb-4">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
            Name
        </label>

        <input
            class="shadow appearance-none border rounded w-fill py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('name') border-red-500 @enderror" name="name" id="name"
            value="{{old('name', $color->name)}}" type="text" required>
    </div>
    <div class="flex items-center justify-between">
        <button id="submit"
                class="bg-green-500 hover:bg-gray-700 text-white" type="submit">Edit</button>
    </div>
</form>
