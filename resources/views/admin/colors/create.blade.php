@extends('layouts.index')
@section('content')

    <h2 class="my-4 text-4xl font-semibold dark:text-gray-400">
        Toevoegen colors
    </h2>
    <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" action="{{ route('colors.store') }}" method="POST">
        @csrf
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                Naam
            </label>
            <input
                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="name" type="text" placeholder="Name" name="name">
        </div>
        <div class="flex items-center justify-between">
            <button id="submit"
                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    type="submit">
                Toevoegen AddressType
            </button>
        </div>
@endsection

