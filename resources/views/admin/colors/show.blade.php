@extends('layouts.index')
@section('topmenu_items')

    <a href="{{ route('colors.index') }}">
        <a href="{{ route('colors.create') }}">
            <button...></button...>
        </a>
        @endsection

        @section('content')
            <div class="max-w-sm bg-white shadow-lg rounded-lg overflow-hidden-4">
                <div class="flex items-center px-6 py-3 bg-gray-900">
                    <h1 class="mx-3 text-white font-semibold text-lh">colors</h1>
                </div>

                <div class="container mx-1">
                    <div class="ml-2 flex flexcol">
                        <h2 class="my-4 text-4xl font-semibold text-gray-600 dark:text-gray-400">
                            colors Admin
                    </div>
                </div>
                @foreach($colors as $color)
                    <tbody>
                    <tr>
                        <td>
                            {{ $color->name }}
                        </td>

                        <td>
                            {{ $color->id }}
                        </td>

                        {{ $color->created_at }}
                        </td>
                    </tr>
                    </tbody>
                    <br>
    @endforeach

@endsection
